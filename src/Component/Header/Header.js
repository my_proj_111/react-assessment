
import './../../App.css';
import logo from './../../Component/Assets/espireInfolabs.png';
import { Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const Header = ({ data }) => {

    return (
        <div className="header-image">
            <div className="App-header">
                <img src={logo} className="App-logo" alt="logo" />
                <p className='header-text text-white p-4'>{data}</p>
            </div>

            <div>

                <Container>
                    <div className='p-1'>
                        <Link to="/" class="btn btn-info ms-4 cursor-pointer" role="button"><strong>Home</strong></Link>

                        <Link to="/about" class="btn btn-info ms-2" role="button"><strong>About Us</strong></Link>

                        <Link to="/user" class="btn btn-info ms-2" role="button"><strong>Users</strong></Link>

                        <Link to="/contact" class="btn btn-info ms-2" role="button"><strong>Contact Us</strong></Link>
                    </div>
                </Container>

            </div>
        </div>
    );
};

export default Header