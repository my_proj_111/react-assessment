const Footer = () => (
    <div className="bg-secondary text-center p-3 text-lg-left text-white">
        <p>© 2024, @copyright</p>
    </div>
);
 
export default Footer;