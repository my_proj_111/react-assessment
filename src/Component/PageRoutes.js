import Header from "./Header/Header";
import AppFooter from "./Footer/AppFooter";
import { Route, Routes } from "react-router-dom";
import Home from "../pages/Home";
import AboutUs from "../pages/AboutUs";
import ContactUs from "../pages/ContactUs";
import UserRedux from "./UserRedux";


function PageRoutes({ data }) {

    return (
        <div>
            <Header data={data}></Header>
            <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/about" element={<AboutUs />} />
                <Route path="/user" element={<UserRedux />} />
                <Route path="/contact" element={<ContactUs />} />
            </Routes>

            {/* <Routes>
                <Route exact path="/" element={<Home />} />
                <Route exact path="/about" element={<AboutUs />} />
                <Route exact path="/user" element={<UserRedux />} />
                <Route exact path="/contact" element={<ContactUs />} />
            </Routes> */}
            <AppFooter />
        </div>
    )
}

export default PageRoutes;