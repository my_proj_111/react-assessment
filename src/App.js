
import { useState } from 'react';
import './App.css';
import PageRoutes from './Component/PageRoutes';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {

  const [data, setData] = useState('React Application Assessment');

  return (
    <div className="bg-image">
      <PageRoutes data={data}></PageRoutes>
    </div>
  );
}

export default App;
