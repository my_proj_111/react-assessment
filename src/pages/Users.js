import React from 'react';
import Table from 'react-bootstrap/Table';

const Users = ({ data, filteredData }) => {

  return (

    <>
      <Table striped hover size="sm">
        <thead>
          <tr>
            <th>User Id</th>
            <th>Name</th>
            <th>Address</th>
            <th>Description</th>
          </tr>
        </thead>
<tbody>
  
{data?.length > 0 ? 
        data?.map((item, index) => {
          
          return (
            
              <tr key={index}>
                <td>{item.userId}</td>
                <td>{item.userName}</td>
                <td>{item.userAddress}</td>
                <td>{item.userDescription}</td>
              </tr>
          )
         
        }) : <div className="fs-5">No Data found</div>
      }
        </tbody>
      </Table>

 </>
  );

};

export default Users;
