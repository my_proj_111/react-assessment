import Container from 'react-bootstrap/Container';
const Home = () => {

    return (
        <Container>
            {/* <div className="fs-2">About Home</div> */}
            <div className='p-3'>
                <p className='fs-3 text-center fw-bold'>React JS - Assessment</p>
                <p><strong>1. Create a React Project with following Routes. Show these routes in the Nav bar in the Header component. In Footer just provide text "Copyright".</strong></p>
                <p>Use Bootstrap to syle header footer of the page.</p>
                <ol type="1">
                    <li>Home</li>
                    <li>About</li>
                    <li>Users</li>
                    <li>Contact</li>
                </ol>

                <p><strong>2. Create a simple Node static Get API using Express with following information:</strong></p>
                <ol type="1">
                    <li>userId</li>
                    <li>userName</li>
                    <li>userAddress</li>
                    <li>userDescription</li>
                </ol>

                <p><strong>
                    3. Call this new api in your React project under User section. Create child components of User section, and use Redux for data flow to the parent components.
                </strong></p>

                <p><strong>
                    4. In User section create a search box to check for any user with name "Neha". If Neha exstis show a div with information:</strong></p>
                <p>Yes, Neha found, otherwise,</p>
                <p>No, Neha not found</p>
                <p><strong>
                    5. You need to store the api name in .env file and use the path name from there.

                </strong></p>
                <p><strong>
                    6. Try to use the reusuable components from the Bootstrap library.
                </strong></p>
            </div>
        </Container>
    )
}

export default Home