import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    data: [],
    filteredData: [],
};

export const UserSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {
        setData: (state, action) => {
            state.data = action.payload;
        },
        setFilteredData: (state, action) => {
            state.filteredData = action.payload;
        },
    },
});

export const { setData, setFilteredData } = UserSlice.actions;
export default UserSlice.reducer;